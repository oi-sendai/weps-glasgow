<?php
/*  Copyright 2013 MarvinLabs (contact@marvinlabs.com)

This program is free software; you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation; either version 2 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program; if not, write to the Free Software
Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA 02110-1301 USA
*/

require_once( CUAR_INCLUDES_DIR . '/core-classes/addon-root-page.class.php' );

if (!class_exists('CUAR_CustomerPrivatePineappleHomeAddOn')) :

/**
 * Add-on to put private pages in the customer area
*
* @author Vincent Prat @ MarvinLabs
*/
class CUAR_CustomerPrivatePineappleHomeAddOn extends CUAR_RootPageAddOn {
	
	public function __construct() {
		parent::__construct( 'customer-private-pineapple-home', '4.0.0', 'customer-private-pineapple' );
		
		$this->set_page_parameters( 600, array(
					'slug'					=> 'customer-private-pineapple-home',
					'parent_slug'			=> 'customer-home',
					'friendly_post_type'	=> 'cuar_private_pineapple',
					'friendly_taxonomy'		=> 'cuar_private_pineapple_category',
					'required_capability'	=> 'cuar_view_pages'
				)
			);
		
		$this->set_page_shortcode( 'customer-area-private-pineapple-home' );
	}
	
	public function get_label() {
		return __( 'Private Pineapple - Home', 'cuar' );
	}
	
	public function get_title() {
		return __( 'Pineapple Home', 'cuar' );
	}		
		
	public function get_hint() {
		return __( 'Root page for the customer pages.', 'cuar' );
	}	

	public function get_page_addon_path() {
		return CUAR_INCLUDES_DIR . '/core-addons/customer-private-pineapple';
	}
}

// Make sure the addon is loaded
new CUAR_CustomerPrivatePineappleHomeAddOn();

endif; // if (!class_exists('CUAR_CustomerPrivatePineappleHomeAddOn')) 
