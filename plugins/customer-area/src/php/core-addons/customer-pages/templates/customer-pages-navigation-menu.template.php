<?php /** Template version: 1.0.0 */ ?>
<?php /*
<nav class="navbar navbar-default" role="navigation">
    <!-- Brand and toggle get grouped for better mobile display -->
    <div class="container">
        <div class="navbar-header">
            <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".cuar-nav-container">
                <span class="sr-only"><?php __('Toggle navigation', 'cuar'); ?></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
                <span class="icon-bar"></span>
            </button>
        </div>        
		
	</div>
</nav>
<?php //echo $this->get_subpages_menu(); ?>
    */ ?>

    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1 class="weps">
            <a href="<?php echo esc_url(home_url()); ?>">
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_small.png" alt="renovations glasgow"/>
              <?php //bloginfo('name'); ?>
            </a>
          </h1> 
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
      </ul>

      <section class="top-bar-section">

<?php            wp_nav_menu( $nav_menu_args ); ?>
          <!-- wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']); -->
      </section>
    </nav>

