<?php /** Template version: 1.0.0 */ ?>

<?php /** @var string $field_name */ ?>
<?php /** @var string $field_label */ ?>

<section class="form-group">
	<div class="row">
		<div class="large-12 colums">
		    <div class="submit-container">
		        <input type="submit" name="<?php echo esc_attr($field_name); ?>" value="<?php echo esc_attr($field_label); ?>" class="btn btn-default" />
		    </div>
		</div>
	</div>
</section>
