<?php /** Template version: 1.0.0 */ ?>
<?php
	$page_classes = array( 'cuar-page-' . $this->page_description['slug'] ); 
	if ( $this->has_page_sidebar() ) $page_classes[] = "cuar-page-with-sidebar";
	else $page_classes[] = "cuar-page-without-sidebar";
	//var_dump($page_classes[0]);
?>
<?php if( $page_classes[0] === "cuar-page-customer-dashboard"):?>
	<h4>http://wp-customerarea.com/documentation/securing-your-private-files/</h4>
<section class="dashboard-navigation">
	<div class="row">
		<div class="small-6 medium-3 columns">
			<article class="card">
		      <a href='<?php echo get_site_url(); ?>/client/paperwork/quotes-and-invoices/'>
		      	<div class="image">
		        	<img src="http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg">
		        	<!-- <span class="title">View Paperwork</span> -->
		      	</div>
		      	<div class="content">
		      		<p>View Paperwork</p>
		        	<!-- <p>We always prioritise work from our existing customers. Please use this form to request an estimation and availability.</p> -->
		      	</div>
		      	<!-- <div class="action"> -->
		      	<!-- </div> -->
		      </a>
		    </article>
		</div>
		<div class="small-6 medium-3 columns">
			<article class="card">
		      <a href='<?php echo get_site_url(); ?>/client/work-undertaken/completed-work/'>
		      	<div class="image">
		        	<img src="http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg">
		        	<!-- <span class="title">Arrange Further Work</span> -->
		      	</div>
		      	<div class="content">
		        	<p>View completed work</p>
		        	<!-- <p>We always prioritise work from our existing customers. Please use this form to request an estimation and availability.</p> -->
		      	</div>
		      	<!-- <div class="action"> -->
		      	<!-- </div> -->
		      </a>
		    </article>
		</div>
		<div class="small-6 medium-3 columns">
			<article class="card">
		      <a href='<?php echo get_site_url(); ?>/contact'>
		      	<div class="image">
		        	<img src="http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg">
		        	<!-- <span class="title">Arrange Further Work</span> -->
		      	</div>
		      	<div class="content">
		        	<p>Arrange Further Work</p>
		        	<!-- <p>We always prioritise work from our existing customers. Please use this form to request an estimation and availability.</p> -->
		      	</div>
		      	<!-- <div class="action"> -->
		      	<!-- </div> -->
		      </a>
		    </article>
		</div>
		<div class="small-6 medium-3 columns">
			<article class="card">
		      <a href='<?php echo get_site_url(); ?>/contact'>
		      	<div class="image">
		        	<img src="http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg">
		        	<!-- <span class="title">Book Guarantee Repair</span> -->
		      	</div>
		      	<div class="content">
		        	<p>Book Guarantee Repair</p>
		        	<!-- <p>All our work is guaranteed against defects for ten years. Please use this form to request a repair you feel is covered by our guarantee.</p> -->
		      	</div>
		      	<!-- <div class="action"> -->
		      	<!-- </div> -->
		      </a>
		    </article>
		</div>
		<div class="small-6 medium-3 columns">
			<article class="card">
		      <a href="<?php echo get_site_url(); ?>/client/my-account/edit-account/" alt="Edit Contact Details">
		      	<div class="image">
		        	<img src="http://static.pexels.com/wp-content/uploads/2014/07/alone-clouds-hills-1909-527x350.jpg">
		        	<!-- <span class="title">Edit Contact Details</span> -->
		      	</div>
		      	<div class="content">
		        	<p>Edit Contact Details</p>
		        	<!-- <p>We use the details we have on file for you to contact you about ongoing work.</p> -->
		      	</div>
		      	<!-- <div class="action"> -->
		      	<!-- </div> -->
		      </a>
		    </article>
		</div>
	</div>
</section>
<?php else: ?>
<section class="cuar-page <?php echo implode( ' ', $page_classes ); ?>">
	<div class="row">
		<div class="large-8 columns">
			<article class="cuar-page-header"><?php 
				$this->print_page_header( $args, $shortcode_content ); ?>
			</article>
		</div>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<article class="cuar-page-content">
				<?php $this->print_page_content( $args, $shortcode_content ); ?>
			</article>
		</div>
		<?php /*
		<div class="large-4 columns">
		<?php if ( $this->has_page_sidebar() ) : ?>	
			<article class="cuar-page-sidebar">
				<?php $this->print_page_sidebar( $args, $shortcode_content ); ?>
			</article>	
		<?php endif; ?>
		</div>
		*/ ?>
	</div>
	<div class="row">
		<div class="large-12 columns">
			<article class="cuar-page-footer">
				<?php $this->print_page_footer( $args, $shortcode_content ); ?>
			</article>
		</div>
	</div>
</section>	
<?php endif;?>
