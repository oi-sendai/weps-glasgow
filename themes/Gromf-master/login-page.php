<?php
/*
* Template Name: Login Page
*/
?>
<?php //get_template_part('templates/page', 'header'); ?>
<div class="row">
	<?php /*
	<header class="client-area-login">
		<img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_small.png" alt="renovations glasgow"/>
	</header>
	*/ ?>
	<section class="home-portal large-12 columns">
		<div class="row">
			<div class="medium-8 medium-offset-2 columns">

				<div class="login-page-wrapper">
					<h1 class="font-size-4">Client Area Login</h1>
					<?php
					$args = array(
						// 'redirect' => get_permalink( get_page( $page_id_of_member_area ) ) 
						 // 'redirect' => site_url( $_SERVER['REQUEST_URI'] ).'/customer-home',  //time your made y
						 'redirect' => get_site_url().'/client', 
					);
					wp_login_form( $args );
					?>
				<!--<img src="pics/toolbar_s.png" alt="Professional Property Maintenance and Renovations" align="center">--><br>
				</div>
			</div>
		</div>

	</section>					
</div>