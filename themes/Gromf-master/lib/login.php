<?php 


/**
 * Hide WP Navigation Bar for non admins
*/
add_action('after_setup_theme', 'remove_admin_bar');

function remove_admin_bar() {
	if (!current_user_can('administrator') && !is_admin()) {
  		show_admin_bar(false);
	}
}


// $args = array(
//         'echo' => true,
//         'redirect' => site_url( $_SERVER['REQUEST_URI'] ), 	//customer-home
//         'form_id' => 'loginform',
//         'label_username' => __( 'Username' ),
//         'label_password' => __( 'Password' ),
//         'label_remember' => __( 'Remember Me' ),
//         'label_log_in' => __( 'Log In' ),
//         'id_username' => 'user_login',
//         'id_password' => 'user_pass',
//         'id_remember' => 'rememberme',
//         'id_submit' => 'wp-submit',
//         'remember' => true,
//         'value_username' => NULL,
//         'value_remember' => false );


// function my_custom_login() {
	
// 	echo '<link rel="stylesheet" type="text/css" href="' . get_bloginfo('stylesheet_directory') . '/login/custom-login-styles.css" />';
// }
// add_action('login_head', 'my_custom_login');

// /*
// By default, the logo links to wordpress.org. You can change this to point to your own site by adding this code to your functions.php file:
// */
// function my_login_logo_url() {
	
// 	return get_bloginfo( 'url' );
// }
// add_filter( 'login_headerurl', 'my_login_logo_url' );
// /*
// Replace “Your Site Name and Info” in line 7 with the name of your site. This is simply ALT text for the logo.
// */
// function my_login_logo_url_title() {
	
// 	return 'Your Site Name and Info';
// }
// add_filter( 'login_headertitle', 'my_login_logo_url_title' );

// /*
// change the error message with this code in your functions.php file:
// */
// function login_error_override()
// {
    
//     return 'Incorrect login details.';
// }
// add_filter('login_errors', 'login_error_override');


// When you enter an incorrect username or password, the login form shakes to alert the user they need to try again.

// function my_login_head() {
	
// 	remove_action('login_head', 'wp_shake_js', 12);
// }
// add_action('login_head', 'my_login_head');

// /*
// Add the following code to your functions.php file so that all users other than admin are automatically redirected:
// */
// function admin_login_redirect( $redirect_to, $request, $user )
// {
	
// 	global $user;
// 	if( isset( $user->roles ) && is_array( $user->roles ) ) {
// 		if( in_array( "administrator", $user->roles ) ) {
		
// 			return $redirect_to;
// 		} else {
			
// 			return home_url();
// 		}
// 	} else {

// 		return $redirect_to;
// 	}
// }
// add_filter("login_redirect", "admin_login_redirect", 10, 3);

// /*
// To leave this box always checked, add this snippet to functions.php:
// */
// function login_checked_remember_me() {
	
// 	add_filter( 'login_footer', 'rememberme_checked' );
// }
// add_action( 'init', 'login_checked_remember_me' );

// function rememberme_checked() {
	
// 	echo "<script>document.getElementById('rememberme').checked = true;</script>";
// }