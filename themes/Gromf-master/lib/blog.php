<?php
/**
 * Register our sidebars and widgetized areas.
 *
 */
function blog_sidebar_widgets_init() {

	register_sidebar( array(
		'name'          => 'Blog Sidebar',
		'id'            => 'blog_sidebar',
		'before_widget' => '<div class="blog-sidebar">',
		'after_widget'  => '</div>',
		'before_title'  => '<h3 class="sidebar-heading">',
		'after_title'   => '</h3>',
	) );

}
add_action( 'widgets_init', 'blog_sidebar_widgets_init' );
?>