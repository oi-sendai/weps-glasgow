<?php
/*
 * Template Name: Contact Us
 */
?>
<?php get_template_part('templates/page', 'header'); ?>

<div class="large-8 large-offset-2 medium-10 medium-offset-1 columns">
<div class
<?php while (have_posts()) : the_post(); ?>
  <article <?php post_class(); ?>>
    <div class="entry-content">
      <?php the_content(); ?>
    </div>
  </article>
<?php endwhile; ?>
<?php /* contact form 7 config
<div class="row">
    <div class="medium-12 columns">
      <label>Name [text Name]</label>
    </div>
  </div>
  <div class="row">
    <div class="medium-6 columns">
      <label>Email [email Email]</label>
    </div>
    <div class="medium-6 columns">
      <label>Telephone [tel Phone]</label>
    </div>
  </div>
  <div class="row">
    <div class="medium-12 columns">
      <label>Preferred contact method [select PreferredContact. "Email" "Telephone Daytime" "Telephone Evening" "Telepathy"]</label>
    </div>
  </div>
  <div class="row">
    <div class="medium-12 columns">
      <label>How can we help? [textarea Message]</label>
    </div>
  </div>
  <div class="row">
    <div class="medium-6 columns">
        [submit class:button class:success class:small "Send"]
    </div>
    <div class="medium-6 columns">
      <label>Attach a file?
        [file File]
      </label>
    </div>
  </div>
  */ ?>