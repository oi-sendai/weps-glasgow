<?php
/*
 * Template Name: Front Page
 */
?>
<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>
<?php get_template_part('templates/page', 'header'); ?>

<div class="row">
	<section class="home-portal medium-4 columns">
	<?php include Wrapper\sidebar_path(); ?>
	</section>
	<section class="home-portal medium-8 columns">
		<div class="row">
			<div class="large-12 columns">
				<h1 class="font-size-4">Local Handyman, Renovations and All-Trades</h1>
				<!--<img src="pics/toolbar_s.png" alt="Professional Property Maintenance and Renovations" align="center">--><br>
			</div>
		</div>
		<div class="row">
			<main class="medium-9 columns">
				<h2  class="font-size-3">We offer a wide range of propert maintenance, repair & renovation services in west and central Glasgow.</h2>
				<h3 class="sub-header">Guarantee</h3>
				<p class="standard">Your job comes with our workmanship guaranteed and we do not require payment until work has been completed. </p><!--for your peace of mind we are insured to work in your home or business premises.-->
				<h3 class="sub-header">All electrical work undertaken.</h3>
				<p class="standard">From a simple light fixture to full home automation we can take care of the details and advise on the best solution for you.</p>
				<h3 class="sub-header">Value</h3>
				<p class="standard">You will be given a fair price for your work and you can be assured that it will be completed to a high standard.
				<a href="customers.html">Click here</a> to read testimonials from previous customers</p>
			</main>
			<aside class="medium-3 columns">
				<p class="not-open">Due to excess demand we are unable to offer a service to new clients at this time. We are taking steps to increase capacity, please check back soon. We apologise for any inconvenience.</p>
				<a href="renovations.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/renovations_link.gif" width="220" border="0" align="right" title="Click for renovations" alt="Send Us a Message" />
				</a>
				<a href="contact.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/message_link.gif" width="220" border="0" align="right" title="Click to send us a message" alt="property maintenance glasgow" />
				</a>
			</aside>
		</div>
		<div class="row">
			<article class="medium-4 columns">
				<a href="gas.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/boiler_link.gif" width="220" border="0" align="left" title="Click for plumbing & gas work info" alt="Combi boiler breakdown" />
				</a>
			</article>
			<article class="medium-4 columns">
				<a href="joinery.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/shelving_link.gif" width="220" border="0" align="top" title="Click for Joinery" alt="Custom shelving & storage Glasgow"/>
				</a>
			</article>
			<article class="medium-4 columns">
				<a href="landlord_services.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/landlord.gif" width="220" border="0" align="right" title="Click for landlord maintenance info" alt="Landlord Maintenance Glasgow" />
				</a>
			</article>
		</div>

		<div class="row">
			<div class="large-12 columns">
				<p align="center">
					<a href="contact.html">Contact us</a> to arrange a free quote or use the menu on the left to navigate our site
				</p>
			</div>
		</div>
	</section>					
</div>