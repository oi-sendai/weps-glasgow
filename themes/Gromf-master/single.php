<div class="row">
	<?php if ( is_active_sidebar( 'blog_sidebar' ) ) : ?>
		<div class="medium-8 columns">
			<?php while (have_posts()) : the_post(); ?>
			  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
			<?php endwhile; ?>
		</div>
		<div class="medium-4 columns">
			<?php dynamic_sidebar( 'blog_sidebar' ); ?>
		</div>
	<?php else: ?>
		<div class="medium-8 medium-offset-2">
			<?php while (have_posts()) : the_post(); ?>
			  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</div>

