<?php

use Roots\Sage\Config;
use Roots\Sage\Wrapper;

?>
<!doctype html>
<html class="no-js" <?php language_attributes(); ?>>
  <?php get_template_part('templates/head'); ?>
  <body <?php body_class(); ?>>
    <!--[if lt IE 9]>
      <div class="alert alert-warning">
        <?php _e('You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.', 'sage'); ?>
      </div>
    <![endif]-->
    <?php if (is_page( 'clients' ) ): ?>
      <!-- client header  -->
      <header>
        <div class="row">
          <div class="large-12 columns">
              <div class="client-login-header">
                <img src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_small.png" alt="renovations glasgow"/>
              </div>
          </div>
        </div>
        <div class="row">
          <div class="large-12 columns">
            <?php include Wrapper\template_path(); ?>
          </div>
        </div>
      </header>
    <?php elseif( cuar_is_customer_area_page() ):?>
      <header class="wrap container client-header" role="document">
        <div class="row">
          <div class="large-12 columns">
            <?php cuar_the_customer_area_menu(); ?>
          </div>
        </div>
      </header>
      <section class="wrap container client-area">
        <div class="row">
          <div class="large-10 medium-12 large-offset-1 columns">
            <?php while (have_posts()) : the_post(); ?>
              <?php get_template_part('templates/page', 'header'); ?>
              <?php get_template_part('templates/content', 'page'); ?>
            <?php endwhile; ?>
          </div>
        </div>        
      </section>
    <?php else: ?>
    <section class="wrap container" role="document">
      <div class="row">
        <div class="large-12 columns">
          <?php do_action('get_header');?>
          <?php get_template_part('templates/header');?>
        </div>
      </div>
      <div class="content row">
          <?php if (Config\display_sidebar()) : ?>
            <aside class="sidebar small-12 medium-3 columns" role="complementary">
              <?php include Wrapper\sidebar_path(); ?>
            </aside><!-- /.sidebar -->
          <?php endif; ?>
          <main class="main small-12 medium-9 columns" role="main">
            <?php include Wrapper\template_path(); ?>
          </main><!-- /.main -->
      </div><!-- /.content -->
    </div><!-- /.wrap -->
    <?php endif;?>
    <?php

      do_action('get_footer');
      get_template_part('templates/footer');
      wp_footer();
    ?>
  </body>
</html>