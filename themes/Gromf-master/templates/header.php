<!-- <div class="contain-to-grid"> -->
  <header>
    <!-- <div class="container"> -->
    <!-- <div class="large-12 columns"> -->
    <nav class="top-bar" data-topbar role="navigation">
      <ul class="title-area">
        <li class="name">
          <h1 class="weps">
            <a href="<?php echo esc_url(home_url()); ?>">
            <img alt="renovations glasgow" src="<?php echo get_template_directory_uri(); ?>/assets/images/logo_small.png">
              &nbsp;
              &nbsp;
              &nbsp;
              <img src="<?php echo get_template_directory_uri(); ?>/assets/images/name_big.png" alt="west glasgow property services"/>
              <?php //bloginfo('name'); ?>
            </a>
          </h1> 
        </li>
        <li class="toggle-topbar menu-icon"><a href="#"><span>Menu</span></a></li>
      </ul>

      <section class="top-bar-section">
        <?php if (has_nav_menu('primary_navigation')) :
          wp_nav_menu(['theme_location' => 'primary_navigation', 'menu_class' => 'nav']);
        endif; ?>
      </section>
    </nav>
    <!-- </div> -->
    <!-- </div> -->
  </header>
<!-- </div> <! - - contain-to-grid -->