<?php if( is_front_page() ):?>
	<section class="other-services">
		<div class="container">
			<div class="row">
			<article class="large-2 columns medium-2 small-4">
				<a href="joinery.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/building-trade/png/sawing-cutting-tool.png" width="118" border="0" align="top" title="JOINERY CLICK HERE" alt="professional joinery and carpentry" />
				</a>
			</article>
			<article class="large-2 columns medium-2 small-4">
				<a href="plumbing.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/building-trade/png/adjusting-wrench-tool.png" width="118" border="0" align="top" title="PLUMBING CLICK HERE" alt="professional plumbing and heating" />
				</a>
			</article>
			<article class="large-2 columns medium-2 small-4">
				<a href="electrical.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/building-trade/png/screwdriver-tool.png" width="118" border="0" align="top" title="ELECTRICAL CLICK HERE" alt="professional electrical repairs and installations" />
				</a>
			</article>
			<article class="large-2 columns medium-2 small-4">
				<a href="decorating.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/building-trade/png/painting-roller-tool-on-a-wall.png" width="118" border="0" align="top" title="DECORATING CLICK HERE" alt="professional painting and decorating" />
				</a>
			</article>
			<article class="large-2 columns medium-2 small-4">
				<a href="tiling.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/building-trade/png/drawing-compass.png" width="118" border="0" align="top" title="TILING CLICK HERE" alt="professional tiling and grouting" />
				</a>
			</article>
			<article class="large-2 columns medium-2 small-4">
				<a href="plastering.html">
					<img src="<?php echo get_template_directory_uri(); ?>/assets/icons/building-trade/png/triangular-small-shovel-tool-for-construction.png" width="118" border="0" align="top" title="PLASTERING CLICK HERE" alt="professional plastering" />
				</a>
			</article>
		</div>
	</section>
<?php endif;?>


<footer class="corporate-footer" role="contentinfo">
  <div class="container">
    <?php dynamic_sidebar('sidebar-footer'); ?>
    <section class="seo-footer">
    	<div class="row">
    		<div class="large-12 columns">
    			<p align="CENTER"><font face="Verdana" size="1" class=nounderline><br><br>
						<a href="handyman.html#appliance">APPLIANCES INSTALLED</a> -
						<a href="handyman.html#audiovideo">AUDIO/VIDEO WIRING</a> -
						<a href="handyman.html#bathroom">BATHROOM WORK</a> -
						<a href="handyman.html#blindscurtains">BLINDS & CURTAINS</a> -
						<a href="business_services.html">BUSINESS MAINTENANCE</a> -
						<a href="buyingselling.html">MOVING</a> -
						<a href="handyman.html#doors">DOORS</a> -
						<a href="handyman.html#airers">DRYING PULLEYS</a> -
						<a href="handyman.html#electrics">ELECTRICS</a> -
						<a href="handyman.html#flatpacks">FLATPACK ASSEMBLY</a> -
						<a href="handyman.html#flooring">FLOORING</a> -
						<a href="handyman.html#joinery">JOINERY</a> -
						<a href="handyman.html#kitchen">KITCHEN FITTING</a> -
						<a href="landlord_services.html">LANDLORD SERVICES</a> -
						<a href="handyman.html#lighting">LIGHTING</a> -
						<a href="handyman.html#locks">LOCKS</a> -
						<a href="handyman.html#oddjobs">ODD JOBS</a> -
						<a href="handyman.html#painting">PAINTING</a> -
						<a href="handyman.html#picturehanging">PICTURE HANGING</a> -
						<a href="handyman.html#plumbing">PLUMBING</a> -
						<a href="handyman.html#repairs">REPAIRS -</a>
						<a href="handyman.html#rubbishremoval">RUBBISH REMOVAL</a> -
						<a href="handyman.html#shelving">SHELVING & STORAGE</a> -
						<a href="handyman.html#telephone">TELEPHONE POINTS</a> -
						<a href="handyman.html#tiling">TILING & GROUTING</a> -
						<a href="handyman.html#tvmount">TV INSTALLATION</a> -
						<a href="handyman.html#papering">WALLPAPERING</a> <br>
						<br>
					</font>
				</p>
    		</div>
    	</div>
    	<div class="row">
    		<div class="large-10 large-offset-1 columns">
    			<div class="row">
    				<div class="medium-3 small-6 columns">
    					<a href="tel:+441415305664">0141 530 5664</a>
    				</div>
    				<div class="medium-3 small-6 columns">
						<a href="mailto:info@wepsglasgow.com">info@wepsglasgow.com</a>
    				</div>
    				<div class="medium-3 small-6 columns">
						<a href="<?php echo esc_url(home_url()); ?>/environmental-policy">Environmental Policy</a>
    				</div>
    				<div class="medium-3 small-6 columns">
						<a href="<?php echo esc_url(home_url()); ?>/terms-and-conditions">Terms &amp; Conditions</a>
    				</div>
    			</div>
				<!--3RD PARTY LINKS-->
				<div class="row">
					<div class="large-3 columns">
						<!-- <p> -->
							<a href="http://www.freeindex.co.uk/categories/property/property_maintenance/" target="_blank">
								<img src="http://www.freeindex.co.uk/fx/link.gif" height="20" width="100" border="0" alt="We are featured in the FreeIndex Property Maintenance directory" />
							</a> -
							<a href="http://www.landlordzone.co.uk/" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/landlordzone.gif" height="20" width="90" border="0" alt="LandlordZone - Landlord resources" />
							</a> - <a href="http://www.dotsco.org" target="_blank">
								<img src="<?php echo get_template_directory_uri(); ?>/assets/images/pics/weresupportingdotsco.gif" height="25" width="60" border="0" alt="Supporting the campaign for a Scottish top level domain" />
							</a>
						<!-- </p> -->
					</div>
				<!-- </div> -->
				<!-- <div class="row"> -->
					<div class="large-9 columns">
						<p align=center>
							<font face="Arial" size="1">
							&copy 2006 - 2012 West End Property Services Scotland Ltd no. 420478 - VAT no. 142 6179 18
							</font>
						</p>
					</div>
				</div>
    		</div>
    	</div>
    </section>
  </div>
</footer>
