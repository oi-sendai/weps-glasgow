<?php get_template_part('templates/page', 'header'); ?>
<div class="row">
	<?php if ( is_active_sidebar( 'blog_sidebar' ) ) : ?>
		<div class="medium-8 columns">
			<?php while (have_posts()) : the_post(); ?>
			  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
			<?php endwhile; ?>
		</div>
		<div class="medium-4 columns">
			<?php dynamic_sidebar( 'blog_sidebar' ); ?>
		</div>
	<?php else: ?>
		<div class="medium-8 medium-offset-2">
			<?php while (have_posts()) : the_post(); ?>
			  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
			<?php endwhile; ?>
		</div>
	<?php endif; ?>
</div>

<?php /*
<div class="row">
	<?php if ( is_active_sidebar( 'blog_sidebar' ) ) : ?>
		<div class="large-8 medium-8 columns">
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'sage'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>

			<?php while (have_posts()) : the_post(); ?>
			  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>
		</div>
		<div class="large-4 hide-for-medium columns">
		
			<?php dynamic_sidebar( 'blog_sidebar' ); ?>
		</div>
	<?php else: ?>
		<div class="medium-8 large-offset-2">
			<?php if (!have_posts()) : ?>
			  <div class="alert alert-warning">
			    <?php _e('Sorry, no results were found.', 'sage'); ?>
			  </div>
			  <?php get_search_form(); ?>
			<?php endif; ?>

			<?php while (have_posts()) : the_post(); ?>
			  <?php get_template_part('templates/content', get_post_type() != 'post' ? get_post_type() : get_post_format()); ?>
			<?php endwhile; ?>

			<?php the_posts_navigation(); ?>
		</div>
	<?php endif; ?>
</div>
*/ ?>